import { storyblokEditable, renderRichText } from "@storyblok/react";

const Paragraph = ({ blok }) => {
  const renderedRichText = renderRichText(blok.content);
  console.debug(renderedRichText)
  return (
    <div className="mb-2" {...storyblokEditable(blok)} key={blok._uid}>
      <div className="text-lg" dangerouslySetInnerHTML={{__html: renderedRichText}}></div>
    </div>
  );
};

export default Paragraph;
