import { storyblokEditable } from "@storyblok/react";
import NextImage from 'next/image'


const Image = ({ blok }) => {
  return (
    <div className="m-2" {...storyblokEditable(blok)} key={blok._uid}>
       <NextImage
          src={blok.asset.filename}
          alt={blok.asset.alt}
          width={1000}
          height={600}
        />
    </div>
  );
};

export default Image;
