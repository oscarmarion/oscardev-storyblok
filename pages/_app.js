import "../styles/tailwind.css";
import { storyblokInit, apiPlugin } from "@storyblok/react";
import Head from "next/head"
import Feature from "../components/Feature";
import Grid from "../components/Grid";
import Page from "../components/Page";
import Teaser from "../components/Teaser";
import Paragraph from "../components/Paragraph";
import Image from "../components/Image";


const components = {
  feature: Feature,
  grid: Grid,
  teaser: Teaser,
  page: Page,
  paragraph: Paragraph,
  image: Image
};

storyblokInit({
  accessToken: "DMQxmT9olnl1wErvdY8NZwtt",
  use: [apiPlugin],
  components,
});

function MyApp({ Component, pageProps }) {
  return <>
   <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>

   <Component {...pageProps} />;

  </>
}

export default MyApp;
