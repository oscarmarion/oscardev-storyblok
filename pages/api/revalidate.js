import {getStoryblokApi} from "@storyblok/react";

export default async function handler(req, res) {

    // if (!req.query.secret || req.query.secret !== process.env.API_REVALIDATE_TOKEN) {
    //     return res.status(401).json({ message: 'Invalid token' });
    // }

    try {
      const storyblokApi = getStoryblokApi();
      let { data } = await storyblokApi.get(`cdn/stories/${req.body?.story_id ?? ''}`);

        const slug = data.story?.full_slug

        if (!!slug) {
            await res.revalidate(`/${slug === 'home' ? '' : slug}`);
            const message = `/${slug} successfully revalidated`;

            console.log(message);
            return res.json({ message });
        }
        const message = `/${slug}  not found`;

        console.log(message);
        return res.status(404).send({ message });
    } catch (err) {
        const message = 'Error revalidating';
        console.log(message, err);
        return res.status(500).send({ message });
    }
}
