import Layout from "../components/Layout";
import {
  useStoryblokState,
  getStoryblokApi,
  StoryblokComponent,
} from "@storyblok/react";

export default function Home({ story }) {
  story = useStoryblokState(story);

  return (
    <Layout>
      <StoryblokComponent blok={story.content} />
    </Layout>
  );
}

export async function getStaticProps(context) {
  // the slug of the story
  let _slug = context.params?.slug ?? "home";

  const slug = Array.isArray(_slug) ? _slug.join('/') : _slug

  let params = {
    version: context.preview || process.env.NODE_ENV === 'development' ? "draft" : "published", // or 'published'
  };

  const storyblokApi = getStoryblokApi();
  let { data } = await storyblokApi.get(`cdn/stories/${slug}`, params);

  return {
    props: {
      story: data ? data.story : false,
      key: data ? data.story.id : false,
    }
  };
}

export async function getStaticPaths() {
  let params = {
    version: "published", // or 'published'
  };
  const storyblokApi = getStoryblokApi();
  let paths = (await storyblokApi.get(`cdn/stories`, params)).data.stories?.map(story => ({
    params: { slug: [story.full_slug ?? story.slug] }
  }));

  return {paths, fallback: 'blocking'}

}
